<?php
    require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Login</title>
</head>
<body>
    <div id="centeredContent">
    <?php
    function displayForm($passNo = "") {
        $form = <<< END
    <form method="POST" enctype="multipart/form-data" ">
        passNo: <input name="passNo" type="text" value="$passNo"><br>
       <input name = "image" type = "file"  />
       
        <input name="submit" type="submit" value="submit">
    </form>
END;
        echo $form;
    }

    if (isset($_POST['submit'])) { // we're receving a submission
       // echo "<p>", print_r($_POST),"</p>";
       // echo "<p>", print_r($_FILES),"</p>";
       // echo "<p>", print_r($_FILES['image']['name']),"</p>";
       
        $passNo = $_POST['passNo'];
      
        $filePath =$_FILES['image']['tmp_name'];
        $imgProperties=getimagesize($filePath);
        $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
    $width=$imgProperties[0];
    $height=$imgProperties[1];
   // $fileName=$_FILES['image']['name'];
    $fileName=$passNo.'.'.$file_ext;
   $target='uploads/'.$fileName;
    move_uploaded_file($filePath,$target);

   
        //  echo '<p>file path is: '.$filePath .'</p>' ;
      //  echo $passNo;
        $absolute_path = realpath($_FILES['image']['name']);
       // echo '<p>file realpath is: '.$absolute_path .'</p>' ;
       
        $file_path ="images/".$passNo .".".$file_ext;
        $file_size =$_FILES['image']['size'];
        $extensions= array("jpeg","jpg","png");
        $errorList = array();

        if(in_array($file_ext,$extensions)=== false){
           $errorList[]="extension not allowed, please choose a JPEG or PNG file.";
        }
        if($file_size > 2097152){
            $errors[]='File size must be excately 2 MB';
         }
         
         if ($errorList) { // STATE 2: submission with errors (failed)
            echo '<ul class="errorMessage">';
            foreach ($errorList as $error) {
                echo "<li>$error</li>\n";
            }
            echo '</ul>';
            displayForm($passNo);
        } else { // STATE 3: submission successful
            $sql = sprintf(
                "INSERT INTO passports VALUES (NULL, '%s','%s')",
                mysqli_real_escape_string($link, $passNo),
                mysqli_real_escape_string($link, $file_path),
              
            );
            if (!mysqli_query($link, $sql)) {
                echo "Fatal ERROR:failed to execute SQL query:" . mysqli_error($link);
            }
            echo "<p> Registration Successful</p>";
        
        }
    } else { // STATE 1: first show
       displayForm();
    }


     
    ?>

    </div>
</body>
</html>