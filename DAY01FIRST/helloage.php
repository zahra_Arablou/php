<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Document</title>
</head>
<body>
<div id="centeredContent">
  
   <?php
function displayForm($name="",$age="")
{
    $form=<<<ENDMARKER
    <form method="POST"  >
       Name:<input name="name" type="text" value=$name> <br>
       Age:<input name="age" type="number" value=$age > <br>
       <input type="submit" value="Say Hello">
    </form>
    ENDMARKER;
    echo $form;
}

if(isset($_POST['name']))
{                 //we are recieving a submission
    $name=$_POST['name'];
    $age=$_POST['age'];
    //VERIFY
    $errorlist=array();
   
    if(strlen($name)<2 || strlen($name)>50){
        
        $errorlist[]="name must be between 2 to 50 char";
    }
    if(filter_var($age,FILTER_VALIDATE_INT)===false || $age<0 || $age>150){
    
       $errorlist[]="Age must be a number between 0 and 150";
   }
   if($errorlist){ //STATE2:SUBMISSION WITH ERRORS (FAILED)
    echo '<ul class="errorMessage">';
    foreach($errorlist as $error)   {
 
        echo "<li>$error</li>\n";
    }
    echo "</ul>";
    displayForm($name,$age);
    }else{//STATE3: submission successful
        echo "Hello $name is $age years old.";
    }
   
}else{ // STATE1: first show
 displayForm();
}

   ?>
</div>
</body>
</html>