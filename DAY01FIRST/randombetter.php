<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form>
        Min:<input name="min" type="text"><br>
        Max:<input name="max" type="text"><br>
        <input type="submit" value="Generate 10 random numbers">
    </form>
    <?php
    $errorlist=array();
    if(!isset($_GET['min']) || !isset($_GET['max']) )
    {
        echo "ERROR: Please enter min and max";
    }else{//submission recieved

      $min=$_GET['min'];
      $max=$_GET['max'];
      //if(!isInt($min))
      if(filter_var($min,FILTER_VALIDATE_INT)===false)
      {
          //array_push($errorlist,"Please enter values for submission");
          $errorlist[]="Minimum must be an integer value";
      }
      if(filter_var($max,FILTER_VALIDATE_INT)===false)
      {
          $errorlist[]="Maximum must be an integer value";
      }
      if($max<$min){
          $errorlist[]="Maximum must not be smaller than minimum";
      }
    }
    if($errorlist){//there were errors - display them
        echo '<ul>';
           foreach($errorlist as $error){
                echo"<li>$error</li>";
            }
            echo '</ul>';
        }else {
          echo "<p>";
          for ($i=0;$i<10;$i++)
          {
              $val=rand($min,$max);
               printf("%s%d",($i==0?"":","),$val);
           } 
           echo "</p>";
        }

    ?>
</body>
</html>