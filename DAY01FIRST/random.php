<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form>
        Min:<input name="min" type="text"><br>
        Max:<input name="max" type="text"><br>
        <input type="submit" value="Generate 10 random numbers">
    </form>
    <?php
    if(!isset($_GET['min']) || !isset($_GET['max']) )
    {
        echo "ERROR: Please enter min and max";
    }else{

      $min=$_GET['min'];
      $max=$_GET['max'];
     echo "<p>";
      for ($i=0;$i<10;$i++)
      {
          $val=rand($min,$max);
          printf("%s%d",($i==0?"":","),$val);
      }
      echo "</p>";
    }

    ?>
</body>
</html>