<?php
require_once 'vendor/autoload.php';
// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true

]];
$app = new \Slim\App($config);
// Fetch DI Container
$container = $app->getContainer();

DB::$dbName = 'day02people';
DB::$user = 'day02people';
DB::$password = 'dogghtxgRA88f41O';
DB::$host = 'localhost';
DB::$port = 3333;

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/cache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1', 'VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};


// Define app routes
$app->get('/hello/{name}/{age}', function ($request, $response, $args) {
    //  return $response->write("Hello " . $args['name']);
    $name = $args['name'];
    $age = $args['age'];
    DB::insert('people', ['name' => $name, 'age' => $age]);
    return $this->view->render($response, 'hello.html.twig', ['name' => $name, 'age' => $age]);
});

//STATE1 :first display of the form
$app->get('/addperson', function ($request, $response, $args) {
    //  return $response->write("Hello " . $args['name']);

    return $this->view->render($response, 'addperson.html.twig');
});

//STATE2&3:RECIEVING SUBMISSION
$app->post('/addperson', function ($request, $response, $args) {
   $name=$request->getParam('name');
   $age=$request->getParam('age');
   $errorList=[];
   if(strlen($name)<2 || strlen($name)>50){
       $errorList[]="Name must be 2 to 50 character long";
   }
   if(filter_var($age,FILTER_VALIDATE_INT)==false || $age<0 || $age>150){
       $errorList[]="Age must be a number between 0 and 150";
       $age="";
   }
   if($errorList){//State2: errors
    //$valuesList=['name'=>$name,'age'=>$age];
    return $this->view->render($response, 'addperson.html.twig',['errorList'=> $errorList]);

   }else{//state 3:success
   
    DB::insert('people', ['name' => $name, 'age' => $age]);
    return $this->view->render($response, 'addperson_success.html.twig');
}
});

//Note:

// Run app
$app->run();
