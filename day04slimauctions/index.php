<?php

session_start();

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');//the system allows us to have multiple logs
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

//always include authentication
$log->pushProcessor(function ($record) {
    $record['extra']['user'] = isset($_SESSION['user']) ? $_SESSION['user']['username'] : '=anonymous=';
    $record['extra']['ip'] = $_SERVER['REMOTE_ADDR'];
    return $record;
});



// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true

]];
$app = new \Slim\App($config);
// Fetch DI Container
$container = $app->getContainer();

if (strpos($_SERVER['HTTP_HOST'], "ipd23.com") !== false) {
    //hosting on ipd23.com database connecton setup
    DB::$dbName = 'cp4996_zahraauctions';
    DB::$user = 'cp4996_zahraauctions';
    DB::$password = 'VGerinTmkemyfuwl';
} else {
    DB::$dbName = 'quiz1database';
    DB::$user = 'quiz1database';
    DB::$password = 'VGerinTmkemyfuwl';
    DB::$host = 'localhost';
    DB::$port = 3333;
}

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    global $log, $container;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // this was tricky to find - getting access to twig rendering directly, without PHP Slim
    http_response_code(500); // internal server error
    $twig = $container['view']->getEnvironment();
    die($twig->render('error_internal.html.twig'));
    //header("Location: /internalerror"); // another possibility, not my favourite
}

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1', 'VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

//Override the default Not Found Handler before creating App
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $response = $response->withStatus(404);
        return $container['view']->render($response, '404.html.twig');
    };
};

// Flash messages handling

$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());

function setFlashMessage($message) {
    $_SESSION['flashMessage'] = $message;
}
// returns empty string if no message, otherwise returns string with message and clears is
function getAndClearFlashMessage() {
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}


function verifyUploadedPhoto(&$photoFilePath, $sellersName)
{
    if (isset($_FILES['photo']) && $_FILES['photo']['error'] != 4) { // file uploaded
        // print_r($_FILES);
        $photo = $_FILES['photo'];
        $filePath =$_FILES['photo']['tmp_name'];
        if ($photo['error'] != 0) {
            return "Error uploading photo " . $photo['error'];
        }
        if ($photo['size'] > 1024 * 1024) { // 1MB
            return "File too big. 1MB max is allowed.";
        }
        $info = getimagesize($photo['tmp_name']);
        //sanitizaing
        // $special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}");
        // $info = str_replace($special_chars, '-', $info);

        if (!$info) {
            return "File is not an image";
        }
        // echo "\n\nimage info\n";
        // print_r($info);
        if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
            return "Width and height must be within 200-1000 pixels range";
        }
        $ext = "";
        switch ($info['mime']) {
            case 'image/jpeg':
                $ext = "jpg";
                break;
            case 'image/gif':
                $ext = "gif";
                break;
            case 'image/png':
                $ext = "png";
                break;
            case 'image/bmp':
                $ext = "bmp";
                break;
            default:
                return "Only JPG, GIF and PNG or bmp file types are allowed";
        }
        //$filename=strtolower(end(explode('.',$_FILES['photo']['name'])));
      //getting filename without the extension
        $exploded_filepath = explode(".", $_FILES['photo']['name']);
        $extension = end($exploded_filepath);
       $fileName=basename($_FILES['photo']['name'], ".". $extension ); 

        $photoFilePath = "uploads/" .  $fileName. "." . $ext;
        $target='uploads/'.  $fileName. "." . $ext;
        move_uploaded_file($filePath,$target);
    }
    return TRUE;
}

// Define app routes
$app->get('/', function ($request, $response, $args) {
    $auctionList = DB::query("SELECT * FROM auctions ORDER BY id DESC");
    return $this->view->render($response, 'index.html.twig', ['list' => $auctionList]);
});
/*$app->get('/newauction/{sellersName}/{sellersEmail}/{itemDescription}/{lastBidPrice}', function ($request, $response, $args) {
    //  return $response->write("Hello " . $args['name']);
    $sellersName = $args['sellersName'];
    $sellersEmail = $args['sellersEmail'];
    $itemDescription = $args['itemDescription'];
    $lastBidPrice = $args['lastBidPrice'];
    DB::insert('auctions', ['sellersName' => $sellersName, 'sellersEmail' => $sellersEmail, 'itemitemDescription' => $itemDescription, 'lastBidPrice' => $lastBidPrice, 'itemImagePath' => 'xxxx']);
    return $this->view->render($response, 'newauction.html.twig', ['sellersName' => $sellersName, 'sellersEmail' => $sellersEmail, 'itemDescription' => $itemDescription, 'lastBidPrice' => $lastBidPrice]);
});*/
//STATE1 :first display of the form
$app->get('/newauction', function ($request, $response, $args) {
    return $this->view->render($response, 'newauction.html.twig');
});

//STATE2&3:RECIEVING SUBMISSION
$app->post('/newauction', function ($request, $response, $args) use ($log) {
    $sellersName = $request->getParam('sellersName');
    $sellersEmail = $request->getParam('sellersEmail');
    $itemDescription = $request->getParam('itemDescription');
    $lastBidPrice = $request->getParam('lastBidPrice');
    $errorList = [];

    $itemDescription = strip_tags($itemDescription, "<p><ul><ul><li><em><strong><i><b><ol><hr><span>");
    
    if (preg_match('/^[a-zA-Z0-9 ,\.-]{2,100}$/', $sellersName) !== 1) {
        $errorList[] = "Seller's name must be 2-100 characters long made up of letters, digits, space, comma, dot, dash";
    }
    if (strlen($itemDescription) < 2 || strlen($itemDescription) > 1000) {
        $errorList[] = "Item description must be 2-1000 characters long";
    }
    if (filter_var($sellersEmail, FILTER_VALIDATE_EMAIL) === false) {
        $errorList[] = "Email doesnot look valid";
    }
    if (!is_numeric($lastBidPrice) || $lastBidPrice < 0 || $lastBidPrice > 99999999.99) {
        $errorList[] = "Initial bid price must be a number between 0 and 99,999,999.99";
    }
     //
     //$valuesList = ['itemDescription' => $itemDescription, 'sellersName' => $sellersName, 
    $photoFilePath = null;
    $retval = verifyUploadedPhoto($photoFilePath, $sellersName);
    
    if ($retval !== TRUE) {
        $errorList[] = $retval; // string with error was returned - add it to list of errors
    }

 //
 $valuesList = ['itemDescription' => $itemDescription, 'sellersName' => $sellersName, 
 'sellersEmail' => $sellersEmail, 'lastBidPrice' => $lastBidPrice,'itemImagePath'=> $photoFilePath];
    if ($errorList) { //State2: errors
       
        return $this->view->render($response, 'newauction.html.twig', ['errorList' => $errorList, 'v' => $valuesList]);
    } else { //state 3:success
        DB::insert('auctions',$valuesList);
        //GLOBAL $LOG;
        $log->debug(sprintf("new Auction created with Id=%s from IP=%s",DB::insertId(),$_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'newauction_success.html.twig');    }
});

// FIXME: handle the case when newBidPrice is not numerical at all, now it causes 404
$app->get('/isbidtoolow/{auctionId:[0-9]+}/{newBidPrice:[0-9\.]+}', function ($request, $response, $args) {
    $oldBidPrice = DB::queryFirstField("SELECT lastBidPrice from auctions WHERE id=%d", $args['auctionId']);
    if ($oldBidPrice == null) {
        echo "Auction not found";
        return;
    }
    $newBidPrice = $args['newBidPrice'];
    if (!is_numeric($newBidPrice) || $newBidPrice < 0 || $newBidPrice > 99999999.99) {
        echo "Bid price must be a number between 0 and 99,999,999.99";
        return;
    }
    if ($newBidPrice <= $oldBidPrice) {
        echo "New bid must be greater than the current bid";
    }
});
// STATE 1: first display of the form
$app->get('/placebid/{id:[0-9]+}', function ($request, $response, $args) {
    $auction = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%d", $args['id']);
    if ($auction) {
        return $this->view->render($response, 'placebid.html.twig', ['a' => $auction]);
    } else { // not found - cause 404 here
        throw new \Slim\Exception\NotFoundException($request, $response);
    }
}); // regex for id

// STATE 2&3: receiving submission
$app->post('/placebid/{id:[0-9]+}', function ($request, $response, $args) use ($log) {
    $biddersName = $request->getParam('biddersName');
    $biddersEmail = $request->getParam('biddersEmail');
    $newBidPrice = $request->getParam('newBidPrice');
    //
    $errorList = [];
    if (preg_match('/^[a-zA-Z0-9 ,\.-]{2,100}$/', $biddersName) !== 1) {
        $errorList[] = "Bidder's name must be 2-100 characters long made up of letters, digits, space, comma, dot, dash";
    }
    if (filter_var($biddersEmail, FILTER_VALIDATE_EMAIL) === false) {
        $errorList[] = "Bidder's email must look like an email";
    }
    $auction = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%d", $args['id']);
    if (!is_numeric($newBidPrice) || $newBidPrice < 0 || $newBidPrice > 99999999.99) {
        $errorList[] = "Initial bid price must be a number between 0 and 99,999,999.99";
    } else {
        if ($auction['lastBidPrice'] >= $newBidPrice) {
            $errorList[] = "The new bid must be higher than the last bid price";
        }
    }
    //
    if ($errorList) { // STATE 2: errors - redisplay the form
        $valuesList = ['biddersName' => $biddersName, 'biddersEmail' => $biddersEmail, 'newBidPrice' => $newBidPrice];
        return $this->view->render($response, 'placebid.html.twig',
                ['errorList' => $errorList, 'a' => $auction, 'v' => $valuesList]);
    } else { // STATE 3: success
        $valuesList = ['lastBidderName' => $biddersName, 'lastBidderEmail' => $biddersEmail, 'lastBidPrice' => $newBidPrice];
        DB::update('auctions', $valuesList, "id=%i", $args['id']);
        // FLASH MESSAGE INSTEAD of success page
        setFlashMessage("Bid placed successfully");
        $log->debug(sprintf("Auction updated with Id=%s from IP=%s",$args['id'],$_SERVER['REMOTE_ADDR']));
        // return $this->view->render($response, 'placebid_success.html.twig');
        return $response->withStatus(302)->withHeader('Location', '/');
    }
});




// Run app - must be the last operation
// if you forget it all you'll see is a blank page
$app->run();
