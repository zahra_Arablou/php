<?php

require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Register</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
           // alert("Jquery works");
            $('input[name=username]').keyup(function() {
                var username = $(this).val();
                //console.log('usename:' + username);
                username = encodeURIComponent(username);
                $('#usernameTaken').load("isusernametaken.php?username=" + username);
            });
        });
   </script>
   </head>
<body>
    <div id="centeredContent">
        <?php
        function displayForm($username = "", $email = "")
        {
            $form = <<< END
    <form method="post">
        Username: <input name="username" type="text" value="$username">
        <span id="usernameTaken" class="errorMessage"></span><br>
        Email: <input name="email" type="email" value="$email"><br>
        Password: <input name="pass1" type="password" ><br>
        password(repeated): <input name="pass2" type="password"><br>
        <input type="submit" value="Say hello">
    </form>
END;
            echo $form;
        }

        if (isset($_POST['username'])) { // we're receving a submission
            $username = $_POST['username'];
            $email = $_POST['email'];
            $pass1 = $_POST['pass1'];
            $pass2 = $_POST['pass2'];
            // verify inputs
            $errorList = array();
            if (preg_match('/^[a-z0-9]{4,20}$/', $username) != 1) {
                $errorList[] = "Username mustbe 4-20 characters long made up of lower-case characters and numbers";
                $username = "";
            } else { //but is this username already in use?
                //FIXME
            }
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                $errorList[] = "Email doesnot look valid";
                $email = "";
            } else {
                //but is the email already in use?
                $result = mysqli_query($link, sprintf(
                    "SELECT * FROM users WHERE email='%s'",
                    mysqli_real_escape_string($link, $email)
                ));
                if (!$result) {

                    echo "SQL Query failed:" . mysqli_error($link);
                    exit;
                }
                $superRecord = mysqli_fetch_assoc($result);
                if ($superRecord) {
                    $errorList[] = "This email is already registered.";
                    $email = "";
                }
            }
            if ($pass1 != $pass2) {
                $errorList[] = "Passwords do not match";
            } else {
                if (
                    strlen($pass1) < 0 || strlen($pass2) > 100
                    || (preg_match("/[A-Z]/", $pass1) == FALSE)
                    || (preg_match("/[a-z]/", $pass1) == FALSE)
                    || (preg_match("/[0-9]/", $pass1) == FALSE)
                ) {
                    $errorList[] = "Password must be 6-100 characters long,"
                        . "With at least one uppercase , one lowercase, and one digit in it";
                }
            }
            //
            if ($errorList) { // STATE 2: submission with errors (failed)
                echo '<ul class="errorMessage">';
                foreach ($errorList as $error) {
                    echo "<li>$error</li>\n";
                }
                echo '</ul>';
                displayForm($username, $email);
            } else { // STATE 3: submission successful
                $sql = sprintf(
                    "INSERT INTO users VALUES (NULL, '%s','%s','%s')",
                    mysqli_real_escape_string($link, $username),
                    mysqli_real_escape_string($link, $email),
                    mysqli_real_escape_string($link, $pass1)
                );
                if (!mysqli_query($link, $sql)) {
                    echo "Fatal ERROR:failed to execute SQL query:" . mysqli_error($link);
                }
                echo "<p> Registration Successful</p>";
                echo '<p> <a href="login.php">Click here to login</a></p>';
            }
        } else { // STATE 1: first show
            displayForm();
        }
        ?>

    </div>
</body>

</html>