<?php
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Add new article</title>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea[name=body]'
        });
    </script>
</head>

<body>
    <div id="centeredContent">
        <?php
        function displayForm($title = "", $body = "")
        { 
            $title = htmlentities($title); // avoid invalid html in case <>" are part of name
            $form = <<< END
    <form method="post">
       Title: <input name="title" type="text" value="$title">
        <textarea name="body" cols="60" rows="10">$body</textarea></br>
        <input type="submit" value="Post article">
    </form>
END;
            echo $form;
        }
        //only logged in users may access this script
      
        if (!isset($_SESSION['blogUser'])) {
            //if user is not authenticated then do not display the form but
            //onluy "access denied " message with link back to index.php
            echo '<p> You must login first to post an article.<a href="index.php">Click to continue</a>.</p>';
        } else {


            if (isset($_POST['title'])) { // we're receving a submission
                $title = $_POST['title'];
                $body = $_POST['body'];
                // FIXME: sanitize body - 1) only allow certain HTML tags, 2) make sure it is valid html
                // WARNING: If you forget to sanitize the body bad things may happen such as JavaScript injection
                $body = strip_tags($body, "<p><ul><li><em><strong><i><b><ol><h3><h4><h5><span>");
                // verify inputs
                $errorList = array();
                if (strlen($title) < 2 || strlen($title) > 100) {
                    array_push($errorList, "Title must be 2-100 characters long");
                    // keep the title even if invalid
                }
                if (strlen($body) < 2 || strlen($body) > 4000) {
                    array_push($errorList, "Body must be 2-4000 characters long");
                    // keep the body even if invalid
                }

                //
                if ($errorList) { // STATE 2: submission with errors (failed)
                    echo '<ul class="errorMessage">';
                    foreach ($errorList as $error) {
                        echo "<li>$error</li>\n";
                    }
                    echo '</ul>';
                    displayForm($title, $body);
                } else { // STATE 3: submission successful
                    $userId = $_SESSION['blogUser']['id'];
                    $sql = sprintf(
                        "INSERT INTO articles VALUES (NULL, '%s',NULL,'%s','%s')",
                        mysqli_real_escape_string($link, $userId),
                        mysqli_real_escape_string($link, $title),
                        mysqli_real_escape_string($link, $body)

                    );
                    if (!mysqli_query($link, $sql)) {
                        die("Fatal ERROR:failed to execute SQL query:" . mysqli_error($link));
                    }
                    $articleId = mysqli_insert_id($link); //the id of the last inserted record
                    echo $articleId;
                    echo "<p>Article Added</p>";
                    echo '<p><a href="article.php?id=' . $articleId . '"> Click here to view it </a></p>';
                }
            } else { // STATE 1: first show
                displayForm();
            }
        }
        ?>

    </div>
</body>

</html>