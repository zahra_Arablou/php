<?php
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Article view</title>

</head>

<body>
<div id="centeredContent">
    <?php
    if (!isset($_GET['id'])) {

        die("Error:missing article ID in the URL");
    }

    $id = $_GET['id'];
    $sql=sprintf("SELECT a.id, a.authorId, a.createdTS, a.title, a.body, u.username 
                        FROM articles as a, users as u WHERE a.id='%s' AND a.authorId = u.id",
            mysqli_real_escape_string($link, $id));
    $result=mysqli_query($link,$sql);
    if(!$result){
        die("SQL Query failed: ". mysqli_error($link));
    }
    $article=mysqli_fetch_assoc($result);
    if($article){
        echo '<div class="articleBox">';
        echo '<h2>' .htmlentities($article['title']).'</h2>';
        $postedDate = date('M d, Y \a\t H:i:s', strtotime($article['createdTS']));
        echo '<i> posted on ' . $postedDate .'</i>';
        echo '<div class="articleBody">'.$article['body'].'</div>\n';
        echo '</div>';
        echo "<script> document.title='" . htmlentities($article['title']) . "'; </script>\n";

    }else{
       echo '<h2>Article not found</h2>'; 
    }
    ?>
</div>
</body>

</html>