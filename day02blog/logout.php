<?php 
require_once 'db.php';
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Logout</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div id="centeredContent">
    <?php

        unset($_SESSION['blogUser']);
    ?>
    <p>You've been logged out. <a href="index.php">Click to continue</a>.</p>
    </div>
</body>
</html>