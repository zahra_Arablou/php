<?php
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register with passport and photo</title>
    <link rel="stylesheet" href="styles.css" />
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea[name=description]'
        });
    </script>
</head>

<body>
    <div id="centeredContent">
        <?php
        function displayForm($description = "", $sellerName = "", $sellerEmail = "", $lastBidPrice = "")
        {
            $sellerName = htmlentities($sellerName); // avoid invalid html in case <>" are part of name
            $form = <<< END
      <form method="post" enctype="multipart/form-data">
       sellers name: <input type="text" name="sellerName" value="$sellerName"><br>
       sellers email: <input type="email" name="sellerEmail" value="$sellerEmail"><br>
       bid price: <input type="number" required min="0"  name="lastBidPrice" step=".01" value="$lastBidPrice"><br>
       Description:<br> <textarea name="description" cols="60" rows="10">$description</textarea></br>
       
       <input type="submit" name="submit" value="submit">
       Photo : <input type="file" name="photo" /><br>
    </form>
END;
            echo $form;
        }

        // returns TRUE on success
        // returns a string with error message on failure
        function verifyUploadedPhoto(&$photoFilePath, $sellerName)
        {
            if (isset($_FILES['photo']) && $_FILES['photo']['error'] != 4) { // file uploaded
                // print_r($_FILES);
                $photo = $_FILES['photo'];
                if ($photo['error'] != 0) {
                    return "Error uploading photo " . $photo['error'];
                }
                if ($photo['size'] > 1024 * 1024) { // 1MB
                    return "File too big. 1MB max is allowed.";
                }
                $info = getimagesize($photo['tmp_name']);
                //sanitizaing
              // $special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}");
              // $info = str_replace($special_chars, '-', $info);
               
                if (!$info) {
                    return "File is not an image";
                }
                // echo "\n\nimage info\n";
                // print_r($info);
                if ($info[0] < 200 || $info[0] > 1000 || $info[1] < 200 || $info[1] > 1000) {
                    return "Width and height must be within 200-1000 pixels range";
                }
                $ext = "";
                switch ($info['mime']) {
                    case 'image/jpeg':
                        $ext = "jpg";
                        break;
                    case 'image/gif':
                        $ext = "gif";
                        break;
                    case 'image/png':
                        $ext = "png";
                        break;
                        case 'image/bmp':
                            $ext = "bmp";
                            break;
                    default:
                        return "Only JPG, GIF and PNG or bmp file types are allowed";
                }
                $photoFilePath = "uploads/" .  $sellerName . "." . $ext;
            }
            return TRUE;
        }

        if (isset($_POST['submit'])) { // are we receiving a submission?
           
            $sellerName=$_POST['sellerName'];
            $sellerEmail=$_POST['sellerEmail'];
            $description = $_POST['description'];
            $lastBidPrice = $_POST['lastBidPrice'];
            $errorList = array();
            //sanitizaing
            $description = strip_tags($description, "<p><ul><ul><li><em><strong><i><b><ol><hr><span>");
            if (preg_match('/^[a-z0-9\,\-\.\ ]{2,100}$/', $sellerName) != 1) {
                $errorList[] = "sellerName mustbe 2-100 characters long,letters (upper/lower-case), space, dash, dot, comma and numbers allowed";
               
            }
            if (filter_var($sellerEmail, FILTER_VALIDATE_EMAIL) === false) {
                $errorList[] = "Email doesnot look valid";
            }
            if ($lastBidPrice<0) {
                $errorList[] = "Price must be greater than 0.";
            }
            // TODO: verify the picture upload is acceptable
            $photoFilePath = null;  // in SQL INSERT query this must become NULL and *not* 'NULL'
            $retval = verifyUploadedPhoto($photoFilePath, $sellerName);
          /*  if($photoFilePath == null)
            {
                $errorList[] = "Please choose a photo";
            }*/
            if ($retval !== TRUE) {
                $errorList[] = $retval; // string with error was returned - add it to list of errors
            }
            // it's okay if no photo was selected - we will just insert NULL value
            //
            if ($errorList) { // STATE 2: errors in submission - failed
                echo "<p>There were problems with your submission:</p>\n<ul>\n";
                foreach ($errorList as $error) {
                    echo "<li class=\"errorMessage\">$error</li>\n";
                }
                echo "</ul>\n";
                displayForm($description , $sellerName , $sellerEmail , $lastBidPrice );
            } else { // STATE 3: successful submission
                if ($photoFilePath != null) {
                    if (move_uploaded_file($_FILES['photo']['tmp_name'], $photoFilePath) != true) {
                        die("Error moving the uploaded file. Action aborted.");
                    }
                }
                $sql = sprintf(
                    "INSERT INTO auctions VALUES (NULL, '%s', '%s', '%s', '%s', %s,NULL,NULL)",
                    mysqli_real_escape_string($link, $description),
                    mysqli_real_escape_string($link, $photoFilePath),
                    mysqli_real_escape_string($link, $sellerName),
                    mysqli_real_escape_string($link, $sellerEmail),
                    mysqli_real_escape_string($link, $lastBidPrice),

                  
                );
                $result = mysqli_query($link, $sql);
                if (!$result) {
                    die("SQL Query failed: " . mysqli_error($link));
                }

                echo "<p>auctions successfully added</p>";
            }
        } else { // STATE 1: first display
            displayForm();
        }

        ?>
    </div>
</body>

</html>