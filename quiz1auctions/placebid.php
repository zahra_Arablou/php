<?php
require_once 'db.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Place bid</title>

</head>

<body>
    <div id="centeredContent">
    <?php
        function displayForm($bidderName = "", $bidderEmail = "", $newBidPrice = "")
        {
            $sellerName = htmlentities($bidderName); // avoid invalid html in case <>" are part of name
            $form = <<< END
      <form method="post" enctype="multipart/form-data">
      bidder Name: <input type="text" name="bidderName" value="$bidderName"><br>
      bidder Email: <input type="email" name="bidderEmail" value="$bidderEmail"><br>
       bid price: <input type="number" required min="0"  name="newBidPrice" step=".01" value="$newBidPrice"><br>
       <input type="submit" name="submit" value="submit">
     
    </form>
END;
            echo $form;
        }?>
        <?php
        if (!isset($_GET['id'])) {

            die("Error:missing auction ID in the URL");
        }

        $id = $_GET['id'];
        $sql = sprintf("SELECT *  FROM auctions WHERE id='%s'",  mysqli_real_escape_string($link, $id));
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die("SQL Query failed: " . mysqli_error($link));
        }
        $auction = mysqli_fetch_assoc($result);
            if($auction){
            echo '<div class="articlePreviewBox">';
            echo "<div>" . $auction['sellersName'] . "</div>";
            echo "<div>" . $auction['lastBidPrice'] . "</div>";
            echo "<p>" . $auction['itemDescription'] . "</p>";
            echo "<img src=" . $auction['itemImagePath'] . " width='150px'>";
            echo "<h2>******************</h2>\n";
        }else {
            die( '<h2>auction not found</h2>');
        }


        if (isset($_POST['submit'])) { // we're receving a submission
            $bidderName = $_POST['bidderName'];
            $bidderEmail = $_POST['bidderEmail'];
            $newBidPrice = $_POST['newBidPrice'];

             // verify inputs
            $errorList = array();
            if (preg_match('/^[a-zA-Z0-9\,\-\.\ ]{2,100}$/', $bidderName) != 1) {
                $errorList[] = "bidderName mustbe 2-100 characters long,letters (upper/lower-case), space, dash, dot, comma and numbers allowed";
               
            }
          
            if (filter_var($bidderEmail, FILTER_VALIDATE_EMAIL) === false) {
                $errorList[] = "Email doesnot look valid";
              
            }
            echo $auction['lastBidPrice'];
            if ($newBidPrice<$auction['lastBidPrice']) {
                $errorList[] = "Price must be greater than lastBidPrice.";
            }
           
            //
            if ($errorList) { // STATE 2: submission with errors (failed)
                echo '<ul class="errorMessage">';
                foreach ($errorList as $error) {
                    echo "<li>$error</li>\n";
                }
                echo '</ul>';
                displayForm($bidderName, $bidderEmail,$newBidPrice);
            } else { // STATE 3: submission successful
                $sql = sprintf("UPDATE auctions SET lastBidPrice = %s , lastBidderName='%s' , lastBidderEmail='%s'  WHERE id=$id",
                mysqli_real_escape_string($link, $newBidPrice),
                    mysqli_real_escape_string($link, $bidderName),
                    mysqli_real_escape_string($link, $bidderEmail)
                   
                );
                if (!mysqli_query($link, $sql)) {
                    echo "Fatal ERROR:failed to execute SQL query:" . mysqli_error($link);
                    exit;
                }
                echo "<p>Updated Successful</p>";
                $_SESSION['bidderName'] = $bidderName;
                $_SESSION['bidderEmail'] = $bidderEmail;
                print_r($_SESSION);
            }
        } else { // STATE 1: first show
            displayForm();
        }

        ?>
    </div>
</body>

</html>