-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Feb 23, 2021 at 07:28 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quiz2auctions`
--

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE `auctions` (
  `id` int(11) NOT NULL,
  `itemDesc` varchar(200) NOT NULL,
  `sellerEmail` varchar(320) NOT NULL,
  `lastBid` decimal(10,2) NOT NULL,
  `lastBidderEmail` varchar(320) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `itemDesc`, `sellerEmail`, `lastBid`, `lastBidderEmail`) VALUES
(1, 'table', 'jerry@jery.com', '0.00', ''),
(2, 'chair', 'ali@ali.com', '0.00', ''),
(3, 'book', 'test@test.com', '0.00', ''),
(4, 'book', 'test@test.com', '0.00', ''),
(5, 'book', 'test@test.com', '1.00', 'hhh@gmail.com'),
(6, 'book', 'test@test.com', '0.00', ''),
(7, 'book', 'test@test.com', '20.00', 'ggg@hhh.com'),
(8, 'book', 'test@test.com', '10.00', 'test@test.com'),
(9, 'book', 'hhh@test.com', '0.00', ''),
(10, 'table', 'jerry@jery.com', '0.00', ''),
(11, 'table', 'jerry@jery.com', '0.00', ''),
(12, 'table', 'jerry@jery.com', '0.00', ''),
(13, 'zahra', 'zahra@gmail.com', '0.00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auctions`
--
ALTER TABLE `auctions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auctions`
--
ALTER TABLE `auctions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
