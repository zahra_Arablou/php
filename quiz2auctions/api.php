<?php

require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$dbName = 'quiz2auctions';
DB::$user = 'quiz2auctions';
DB::$password = 'RXSRxLp3XEsf0V3g';
DB::$host = 'localhost';
DB::$port = 3333;

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params) {
    global $log, $container;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // this was tricky to find - getting access to twig rendering directly, without PHP Slim
    http_response_code(500); // internal server error
    header('Content-type: application/json; charset=UTF-8');
    die(json_encode("500 - Internal error"));
}

// Create and configure Slim app

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);
$container = $app->getContainer();

//Override the default Not Found Handler before creating App
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    };
};
// set content-type globally using middleware (untested)
$app->add(function($request, $response, $next) {
    sleep(1); // artificially delay all responses by 1 second
    $response = $next($request, $response);
    return $response->withHeader('Content-Type', 'application/json');
});
// API calls handlers are below
$app->get('/', function (Request $request, Response $response, array $args) {
    $response->getBody()->write("Auction app with RESTful API");
    return $response;
});

$app->get('/auctions', function (Request $request, Response $response, array $args) {
  
   // $queryParams = $request->getQueryParams();
  
    $list = DB::query("SELECT * FROM auctions ");
    $json = json_encode($list, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});

// fetch one record
$app->get('/auctions/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
   
    $item = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%i ", $args['id']);
    if (!$item) {
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    $json = json_encode($item, JSON_PRETTY_PRINT);
    $response->getBody()->write($json);
    return $response;
});
$app->post('/auctions', function (Request $request, Response $response, array $args) use ($log) {
    
    $json = $request->getBody();
    $item = json_decode($json, TRUE); // true makes it return an associative array instead of an object
    // validate
    if ( ($result = postvalidateauction($item)) !== TRUE) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
    $item['lastBidderEmail']='';
    $item['lastBid']=0;
 
    DB::insert('auctions', $item);
    $insertId = DB::insertId();
    $log->debug("Record auctions added id=" . $insertId);
    $response = $response->withStatus(201);
    $response->getBody()->write(json_encode($insertId));
    return $response;
});

$app->patch('/auctions/{id:[0-9]+}', function (Request $request, Response $response, array $args) use ($log) {
  
    $json = $request->getBody();
    $item = json_decode($json, TRUE); // true makes it return an associative array instead of an object
    // auction: validate
   // $method = $request->getMethod();
   $origItem = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%i", $args['id']);
   $lastbid=$item['lastBid'];
    if ( ($result = patchvalidateauction($item,$lastbid)) !== TRUE) {
        $response = $response->withStatus(400);
        $response->getBody()->write(json_encode("400 - " . $result));
        return $response;
    }
   // $origItem = DB::queryFirstRow("SELECT * FROM auctions WHERE id=%i", $args['id']);
    if (!$origItem) { // record not found
        $response = $response->withStatus(404);
        $response->getBody()->write(json_encode("404 - not found"));
        return $response;
    }
    DB::update('auctions', $item, "id=%i", $args['id']);
    $response->getBody()->write(json_encode(true)); // JavaScript clients (web browsers) do not like empty responses
    return $response;
});



function postvalidateauction($auction, $forPatch = false) {
    if ($auction === NULL) { // probably json_decode failed due to JSON syntax errors
        return "Invalid JSON data provided";
    }
    // - only allow the fields that must/can be present
    $expectedFields = ['itemDesc', 'sellerEmail'];
    $auctionFields = array_keys($auction); // get names of fields as an array
    // check if there are any fields that should not be there
    if ($diff = array_diff($auctionFields, $expectedFields)) {
        return "Invalid fields in auction: [". implode(',', $diff). "]";
    }
    //
   
        // - check if any fields are missing that must be there
        if ($diff = array_diff($expectedFields, $auctionFields)) {
            return "Missing fields in auction: [". implode(',', $diff). "]";
        }
   
    // do not allow any fields to be null - database would not accept it
    $nullableFields = []; // put list of nullable fields here
    foreach($auction as $key => $value) {
        if (!in_array($key, $nullableFields)) {
            if (@is_null($value)) { // @ is to suppress a warning (which would be printed out)
                return "$key must not be null";
            }
        }
    }
      // - itemDesc 1-100 characters long
      if (isset($auction['itemDesc'])) {
        $itemDesc = $auction['itemDesc'];
        if (strlen($itemDesc) < 1 || strlen($itemDesc) > 100) {
            return "itemDesc description must be 1-100 characters long";
        }
    }
      // - sellerEmail a valid Email
      if (isset($auction['sellerEmail'])) {
        $sellerEmail = $auction['sellerEmail'];
        if (!filter_var($sellerEmail, FILTER_VALIDATE_EMAIL)) {
                 return "seller Email has invalid format";
        }
     }
       
   
     // if we passed all tests return TRUE
    return TRUE;
}

function patchvalidateauction($auction,$lastbid) {
    if ($auction === NULL) { // probably json_decode failed due to JSON syntax errors
        return "Invalid JSON data provided";
    }
    // - only allow the fields that must/can be present
    $expectedFields =  ['lastBid', 'lastBidderEmail'];
    $auctionFields = array_keys($auction); // get names of fields as an array
    // check if there are any fields that should not be there
    if ($diff = array_diff($auctionFields, $expectedFields)) {
        return "Invalid fields in auction: [". implode(',', $diff). "]";
    }
    //
   
    // do not allow any fields to be null - database would not accept it
    $nullableFields = []; // put list of nullable fields here
    foreach($auction as $key => $value) {
        if (!in_array($key, $nullableFields)) {
            if (@is_null($value)) { // @ is to suppress a warning (which would be printed out)
                return "$key must not be null";
            }
        }
    }
    if (isset($auction['lastBidderEmail'])) {
        $lastBidderEmail = $auction['lastBidderEmail'];
        if (!filter_var($lastBidderEmail, FILTER_VALIDATE_EMAIL)) {
                 return "lastBidderEmail Email has invalid format";
        }
     }
     if (isset($auction['lastBid'])) {
        $newBid = $auction['lastBid'];
        if ($lastbid>$newBid) {
                 return "bid error";
        }
     }
    // if we passed all tests return TRUE
    return TRUE;
}

// Run app - must be the last operation
// if you forget it all you'll see is a blank page
$app->run();